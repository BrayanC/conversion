# Convertir grados centigrados a grados farenheit.

# Autor: Brayan Gonzalo Cabrera Cabrera.

# brayan.cabrera@unl.edu.ec

centigrados= (float) (input ("Ingrese el número en grados centigrados="))

farenheit = (centigrados * 9/5)+32

print ("La temperatura en grados farenheit es de: ", farenheit)

